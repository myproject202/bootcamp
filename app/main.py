from flask import Flask
import os
import socket

app = Flask(__name__)

@app.route('/hostname', methods=['GET'])
def get_hostname():
    hostname = socket.gethostname()
    return hostname

@app.route('/author', methods=['GET'])
def get_author():
    author = os.environ.get('AUTHOR')
    if author:
        return author
    return 'No author found'

@app.route('/id', methods=['GET'])
def get_id():
    id = os.environ.get('UUID')
    if id:
        return id
    return 'No id found'

if __name__ == "__main__":
    app.run(debug=True, port=3000, host="0.0.0.0")
